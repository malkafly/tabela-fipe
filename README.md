ORIGINAL: http://phpbrasil.com/script/QqiPgvQWXrX3/importando-indices-da-fipe-preco-automoveis-para-seu-banco

Importando índices da FIPE (preço automóveis) para seu banco
criado por Ronaldo Moreira Junior em 27/07/2011 10:06am



--- EDIT (24/08/2016): Gostaria de deixar um agradecimento a todos que contribuíram neste script, abaixo a lista:

* Rafael Piza - Este em especial, que ainda da manutenção no código, valeu irmão
* Bruno Javan
* Marciel Barcellos
* João Cason
* Deivid Fortuna

Se alguém topar, podemos criar o projeto no github, para ficar versionado e também para organizar o código em PSR-2

--- EDIT (30/05/2014): Pô galera, deixa ao menos um joinha aí por favor. O script ta desde 2011, pessoal só faz download e nem agradece =( Pensa quanto trabalho vc economizou com estre script =)

Olá pessoal!

Depois de 5 anos postarei um script novamente no phpBrasil.

Vamos ao que interessa.
Este script pega todas marcas, modelos e e ano dos modelos com seus respectivos valores corrigidos pela FIPE, que é o orgão responsável pela pesquisa da média dos valores dos automóveis no Brasil. O script foi desenvolvido para um cliente que faz avaliações de veículos e os revende, logo, o cliente pediu os preços da FIPE em seu sistema, sem abrir o site da FIPE.

Praticamente (99%) todas lojas, garagens e concessionárias usam a tabela da FIPE como referência para calcular o valor dos veículos a serem negociados.

Este script pega apenas os carros, se você quiser pegar motos e cainhões, modifique a variável $_P = 51 para 52 (motos) e 53 (caminhões).

O script demora a terminar a execução, pois são mais de 18 mil registros a serem extraidos, mas você pode acompanhar a inserção dos registros direto no seu banco ou interface de gerenciamento como phpMyAdmin, etc.

Execute o script extrair.php e espere terminar a execução, é simples. Junto do script tem o sql da tabela para onde ele importa, um arquivo de funções e outra classe para o banco.

Eu desenvolvi esse script pois o pessoal da FIPE queriam me cobrar 3 mil reais por mês para ter acesso ao banco de dados.

*** NOTA: Observe a data da postagem deste script, caso a FIPE altere o layout do site, o script provavelmente não funcionará mais. Use por sua conta e risco, qualquer responsabilidade é de quem usar, não minha.

Espero que seja útil.